# Morello bootstrap scripts

## Setting up the build environment

    $ apt install git build-essential python3

If cross compiling on x86 you also need:

    $ apt install libtinfo5 linux-libc-dev-arm64-cross

## Building
### Quick build:

    $ cd morello-aarch64/morello
    $ ./scripts/build-all.sh

Which will perform a full cross build on aarch64 host. You can optionally pass `--x86_64` to `build-all.sh` to do a cross-build from an x86_64 host.

On success, the purecap applications are available at `morello-aarch64/morello/examples/bin` and the morello busybox rootfs is available at `morello-aarch64/morello/morello-rootfs`.

### Step by step build execution
In `morello-aarch64/morello`:

1. `env/morello-export-env-variables`: set up $PATH for the Morello toolchain \
Sourcing this sets up $PATH where Morello supporting LLVM overshadows the system one (since not upstreamed yet).

2. `scripts/build-all.sh`: Runs all the following scripts in sequence \
Accepts either `--x86_64` or `--aarch64`. The default is `--aarch64` and is assumed if neither is specified. \
The `--aarch64` switch downloads and builds everything from an aarch64 host, passing `--x86_64` assumes an x86\_64 host

3. `scripts/fetch-toolchain.sh`: Download required llvm toolchain (clang+compiler_rt+crt_objects)

4. `scripts/build-musl.sh`: Download the musl libc source and build it without libshim.

5. `tools/Makefile`: Compile `morello_elf` \
This utility sets up the ELF headers of the Morello binary. Necessary since the ELF format is not finalized yet.

6. `examples/test-app/Makefile`: Compile a hello world program \
Compiles a simple hello world for the Morello architecture and passes it to morello\_elf. Output is in `examples/bin`.

7. `examples/morello-heap-app/Makefile`: Compile a capability based heap test program \
Compiles a simple capability based heap test for the Morello architecture and passes it to morello\_elf. Output is in `examples/bin`.

8. `examples/morello-stack-app/Makefile`: Compile a capability based stack test program \
Compiles a simple capability based stack test for the Morello architecture and passes it to morello\_elf. Output is in `examples/bin`.

9. `scripts/build-busybox`: Fetch the morello busybox source and build the morello busybox rootfs.

10. `scripts/build-rootfs`: Package the built examples in the built morello busybox rootfs.

**NOTE** : To see the differences in behavior in between aarch64 and Morello, it is possible to recompile the same program for aarch64 and compare the results with what happens on Morello. This should point out that just recompiling the same code on Morello makes it more robust and secure.
Output for Morello is in `examples/bin`.

**NOTE** : To add and compile a new example to test, add a `main.c` program file to `examples/morello-<example_name>` and also add a `Makefile` parallel to the program file. Refer Makefile in other examples (mentioned in point 6,7,8 above) and replicate the same for the new example. To add this example in the execution flow, add the program compilation (make) to `scripts/build-all.sh` and to package it in the morello busybox rootfs, add the `<example_name>` to `purecap_app_name_appendix` list in `scripts/build-rootfs.sh`.

Please refer to the following links to have a clear understanding of the implemented process:

* refer to `man gcc` and look up each argument
* refer to the [gcc docs](https://gcc.gnu.org/onlinedocs/gcc/) and read through the chapters on Standards and Standard Libraries
* refer to [this gentoo doc](https://dev.gentoo.org/~vapier/crt.txt) for an explanation of what all the crt files do
* refer to musl's source code for what the other `crt`s do
(crt = C RunTime)
