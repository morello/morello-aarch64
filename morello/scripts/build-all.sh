#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -u

MODE="aarch64"

help () {
cat <<EOF
Usage: $0 [options]

OPTIONS:
  --aarch64   build on an aarch64 host [DEFAULT]
  --x86_64    build on an x86_64 host
EOF
exit 0
}

main () {

	for arg ; do
	case $arg in
		--aarch64) MODE="aarch64" ;;
		--x86_64) MODE="x86_64" ;;
		--help|-h) help ;;
	esac
	done

	if [ "$MODE" = "aarch64" -a $(uname -m) != "aarch64" ]; then
		echo "ERROR: attempting an aarch64 cross build NOT on an arm cpu";
		exit 1
	fi

	source "./env/morello-export-env-variables" "$MODE"

	# Cleanup old files
	rm -fr "${EXAMPLES_BIN}" "${MUSL_BIN}" "${MORELLO_TAR_IMG}"

	# fetch toolchain
	if [ ! -d "$TOOLCHAIN_DIR" ]; then
		./scripts/fetch-toolchain.sh
	fi

	# Fetch and build musl libc
	${CURR_DIR}/scripts/build-musl.sh

	# Build morello_elf
	make -C "${CURR_DIR}/tools"

	# Create examples/bin
	mkdir -p "${EXAMPLES_BIN}"

	# Build test-app
	make -C "${CURR_DIR}/examples/test-app"

	# Build morello-heap-app
	make -C "${CURR_DIR}/examples/morello-heap-app"

	# Build morello-stack-app
	make -C "${CURR_DIR}/examples/morello-stack-app"

	if [ ! -d "$MORELLO_PROJECTS" ]; then
		mkdir -p "$MORELLO_PROJECTS"
	fi

	# Build morello busybox
	${CURR_DIR}/scripts/build-busybox.sh

	# Create rootfs of morello busybox
	${CURR_DIR}/scripts/build-rootfs.sh
}

time main "$@"
