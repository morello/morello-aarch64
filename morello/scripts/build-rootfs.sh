#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -u

# app names will be appended
readonly purecap_app_name_appendix=("heap" "helloworld" "stack")

for str in "${purecap_app_name_appendix[@]}"; do
    echo "copying morello-$str into busybox rootfs"
    cp "$EXAMPLES_BIN/morello-$str" "$MORELLO_ROOTFS/bin/"
done

tar -czvf "$MORELLO_TAR_IMG" morello-rootfs
