#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -u

source ./env/morello-repos-versions

readonly MORELLO_LINUX_KERNEL_HEADER="${MORELLO_PROJECTS}/morello-linux-headers/usr"

# Fetch morello busybox
if [ ! -d "$MORELLO_BUSYBOX_SRC" ]; then
    git clone "$MORELLO_BUSYBOX_URL" -b "$MORELLO_BUSYBOX_BRANCH_TAG" "${MORELLO_PROJECTS}/morello-busybox"
fi

# Fetch morello linux header
if [ ! -d "$MORELLO_LINUX_KERNEL_HEADER" ]; then
    git clone "$MORELLO_LINUX_KERNEL_HEADER_URL" -b "$MORELLO_LINUX_KERNEL_HEADER_BRANCH_TAG" "${MORELLO_PROJECTS}/morello-linux-headers"
fi

mkdir -p "${MORELLO_ROOTFS}"

# Build morello-busybox
cp "${MORELLO_BUSYBOX_SRC}/configs/morello_busybox_defconfig" "${MORELLO_BUSYBOX_SRC}/.config"

make clean -C "${MORELLO_BUSYBOX_SRC}"
make -j$NCORES -C "${MORELLO_BUSYBOX_SRC}"
make CONFIG_PREFIX="${MORELLO_ROOTFS}" install -C "${MORELLO_BUSYBOX_SRC}"
