#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -u

source ./env/morello-repos-versions

# Fetch musl source
if [ ! -d "$MUSL_SRC_DIR" ]; then
	git clone "$MUSL_SRC_URL" -b "$MORELLO_MUSL_BRANCH_TAG"
fi

mkdir -p "${MUSL_BIN}"

# Build musl
cd musl-libc
CC=clang ./configure \
	--disable-shared \
	--enable-morello \
	--disable-libshim \
	--target=aarch64-linux-gnu \
	--prefix="${MUSL_BIN}"
cd -

if [ "$?" != 0 ]; then
    exit 1
else
    make -j$NCORES -C "musl-libc"
    make install -C "musl-libc"
fi
