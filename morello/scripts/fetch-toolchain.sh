#!/usr/bin/env bash

# SPDX-License-Identifier: BSD-3-Clause

set -u

source ./env/morello-repos-versions

MODE="${MODE:-aarch64}"

if [ ! -d "$TOOLCHAIN_DIR" ]; then
    mkdir -p "$TOOLCHAIN_DIR"
fi

cd "$TOOLCHAIN_DIR"

if [ "$BRANCH_NAME" == "mainline" ]; then
    # Always fetch for aarch64 to bind for native build support
    wget "$MORELLO_TOOLCHAIN_URL_AARCH64"
    # Fetch toolchain
    if [ "$MODE" = "aarch64" ]; then
        unzip "${MORELLO_TOOLCHAIN_URL_AARCH64##*/}"
        tar -xf llvm-morello-linux-aarch64-clang.tar.gz
    elif [ "$MODE" = "x86_64" ]; then
        # Rename the aarch64 fetched toolchain to package for native build
        mv "${MORELLO_TOOLCHAIN_URL_AARCH64##*/}" aarch64_tc.zip
        # Fetch for x86_64 based cross build
        wget "$MORELLO_TOOLCHAIN_URL_X86_64"
        unzip "${MORELLO_TOOLCHAIN_URL_X86_64##*/}"
        tar -xf llvm-morello-linux-x86-clang.tar.gz
        mv llvm-morello-linux-x86 llvm-morello-linux-x86_64
    fi
else
    # Always fetch for aarch64 to bind for native build support
    wget -O llvm-morello-linux-aarch64-clang.tar.gz "$MORELLO_TOOLCHAIN_URL_AARCH64"
    zip aarch64_tc.zip llvm-morello-linux-aarch64-clang.tar.gz
    if [ "$MODE" = "x86_64" ]; then
        wget -O llvm-morello-linux-x86-clang.tar.gz "$MORELLO_TOOLCHAIN_URL_X86_64"
        tar -xf llvm-morello-linux-x86-clang.tar.gz
    fi
fi

cd -
