# SPDX-License-Identifier: BSD-3-Clause

CC=clang
ELF_PATCH=morello_elf
CMP=cmp -l
GAWK=gawk
MUSL_LIB=../../musl-bin/lib
CLANG_RESOURCE_DIR=$(shell clang -print-resource-dir)

OUT=../bin
# we want the same result no matter where we're cross compiling (x86_64, aarch64)
TARGET?=aarch64-linux-gnu

all:
	mkdir -p $(OUT)
	$(CC) -c -g -nostdinc -isystem ../../musl-bin/include \
		-march=morello+c64 -mabi=purecap main.c -o $(OUT)/morello-stack.c.o \
		--target=$(TARGET) -fno-stack-protector
	$(CC) --target=$(TARGET) -fuse-ld=lld -march=morello+c64 -mabi=purecap \
		$(MUSL_LIB)/crt1.o \
		$(MUSL_LIB)/crti.o \
		$(CLANG_RESOURCE_DIR)/lib/aarch64-unknown-linux-musl_purecap/clang_rt.crtbegin.o \
		$(OUT)/morello-stack.c.o \
		$(CLANG_RESOURCE_DIR)/lib/aarch64-unknown-linux-musl_purecap/libclang_rt.builtins.a \
		$(CLANG_RESOURCE_DIR)/lib/aarch64-unknown-linux-musl_purecap/clang_rt.crtend.o \
		$(MUSL_LIB)/crtn.o \
		-nostdlib -L$(MUSL_LIB) -lc -o $(OUT)/morello-stack -static \
		-fno-stack-protector
	$(ELF_PATCH) $(OUT)/morello-stack

clean:
	rm $(OUT)/morello-stack.c.o
	rm $(OUT)/morello-stack
